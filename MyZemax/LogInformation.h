#pragma once
#include "Includes.h"
#include "Point.h"
#include "Beam.h"

class LogInformation
{
public:

	Beam beam;
	mpreal time;
	mpreal refractiveIndex;

	LogInformation(Beam, mpreal, mpreal);
};

