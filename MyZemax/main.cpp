#include "Includes.h"
#include "OpticalSurface.h"
#include "Modeling.h"


int main()
{
	auto something = mpfr_buildopt_tls_p();
	mpreal::set_default_prec(500);

	Vector2 satspeed = Vector2(0,0);

	SurfCoeffs circle = SurfCoeffs({ 1, 1, 0, 0, 0, 1 });

	SurfCoeffs smallCircle = SurfCoeffs({ 1, 1, 0, 0, 0, 2 });

	SurfCoeffs final = SurfCoeffs({ 0, 0, 0, 0, 0.5, 0.1 });

	SchottCoeffs innerCoeffs = { 1, 1, 1, 1, 1, 1 };

	SchottCoeffs extCoeffs = { 1, 1, 1, 1, 1, 1 };

	SurfType extMenisk = SurfType(Characteristic::Common, extCoeffs);

	SurfType innerMensik = SurfType(Characteristic::Common, innerCoeffs);

	SurfType mirrorMenisk = SurfType(Characteristic::Mirror, extCoeffs);

	std::vector<Beam> beams = std::vector<Beam>();
	for (size_t i = 0; i < 10000000; i++)
	{
		beams.push_back(Beam(Point(-0.01 + 0.02*(i - 1) / 1000000, -1), Vector2(0, 1), 532 * pow(10, -9)));
	}
	beams.push_back(Beam(Point(-0.01, -0.1), Vector2(0, 1), 532 * pow(10, -9)));

	OpticalSurface external1 = OpticalSurface(circle, extMenisk, satspeed);
	OpticalSurface external2 = OpticalSurface(circle, mirrorMenisk, satspeed);
	OpticalSurface internal1 = OpticalSurface(smallCircle, innerMensik, satspeed); 
	OpticalSurface finale = OpticalSurface(final, extMenisk, satspeed);
	modeling({ external1, internal1, internal1, external2, internal1, internal1, external1, finale }, beams);
	system("pause");
	return 0;
}

