#pragma once

#include "SurfCoeffs.h"
#include "SurfType.h"
#include "Beam.h"
#include "Point.h"
#include "LogInformation.h"

class OpticalSurface
{
public:
	const SurfType const surfParameters;
	const Vector2 const surfSpeed;
	const SurfCoeffs const coeffsOriginal;

	OpticalSurface(SurfCoeffs, SurfType, Vector2);

	LogInformation beamPassing(Beam &beam, mpreal currentTime, mpreal currentRIndex) const;

	mpreal _SchottFormula(mpreal lamb) const;
	
	std::tuple<Point, mpreal> timeAndPointOfCross(const SurfCoeffs surfCoeffs, const Beam &beam, mpreal n) const;

	void signsAndPhi(const Beam &beam, mpreal &signCosPhi, mpreal &signSinPhi) const;

	Vector2 normalToSurface(const SurfCoeffs surfCoeffs, const Point &crossPoint) const;

	const SurfCoeffs currentTimeRecording(mpreal currentTime) const;
};

