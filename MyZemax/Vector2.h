#pragma once
#include "Includes.h"

class Vector2
{
public:
	mpreal x, y;
	Vector2();
	Vector2(mpreal, mpreal);
	
	static mpreal dot(const Vector2 &v1, const Vector2 &v2);
	Vector2 normalize() const;
	mpreal module() const;
	Vector2 perpendiculize();
	Vector2 operator/(const mpreal & a) const;
	Vector2 operator*(const mpreal & a) const;
	Vector2 operator+(const Vector2 & a) const;
};

