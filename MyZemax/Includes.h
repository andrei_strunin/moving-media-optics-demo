#pragma once
#define _USE_MATH_DEFINES
#define speedOfLight 299792458;
#include <iostream>
#include <iomanip>  
#include <fstream>
#include <cmath>
#include <cstdlib>
#include <mpreal.h>
#include <string>
#include <vector>
#include <array>
#include <tuple>

using namespace mpfr;