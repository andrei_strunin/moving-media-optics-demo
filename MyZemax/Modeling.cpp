#include "Modeling.h"

void modeling(std::vector<OpticalSurface> surfaces, std::vector<Beam> beams) 
{		
	
	unsigned int threads_count = std::thread::hardware_concurrency();
	threads_count = 1;
	std::vector<std::thread> calc_threads = {};
	for (size_t i = 0; i < threads_count; i++)
	{
		int len = beams.size() / threads_count;
		if (i != threads_count - 1)
			calc_threads.push_back(std::thread(processing, i, i * len, len, beams, surfaces));
		else 
			calc_threads.push_back(std::thread(processing, i, i * len, len + beams.size() % threads_count, beams, surfaces));
	}
	for (size_t i = 0; i < threads_count; i++)
	{
		calc_threads[i].join();
	}
	
	
	/*for (int i = 0; i < beams.size(); i++)
	{
		currentTime = 0;
		currentRIndex = 1;
		LogInformation data = LogInformation(beams[i], 0, 1);
		dataPrint(data, f, i, 0);
		for (int j = 0; j < surfaces.size(); j++)
		{
			data = surfaces[j].beamPassing(beams[i], currentTime, currentRIndex);
			dataPrint(data, f, i, j + 1);
			currentTime += data.time;
			currentRIndex = data.refractiveIndex;
		}
	}*/
}

void processing(int i, int start, int len, std::vector<Beam> beams, std::vector<OpticalSurface> surfaces) {
	mpreal::set_default_prec(100);
	std::fstream f;
	f.open("data" + std::to_string(i) + ".txt", std::fstream::in | std::fstream::out | std::fstream::trunc);
	for (size_t i = start; i < start + len; i++)
	{
		mpreal currentTime, currentRIndex;
		currentTime = 0;

		//this means that ray starts in vacuum
		currentRIndex = 1;

		LogInformation data = LogInformation(beams[i], 0, 1);
		dataPrint(data, f, i, 0);
		for (int j = 0; j < surfaces.size(); j++)
		{
			data = surfaces[j].beamPassing(beams[i], currentTime, currentRIndex);
			dataPrint(data, f, i, j + 1);
			currentTime += data.time;
			currentRIndex = data.refractiveIndex;
		}
		if (i % 1000 == 0) std::cout << i << std::endl;
	}
	f.close();
	mpfr_free_cache();
}
std::mutex print_mutex;

void dataPrint(LogInformation data, std::fstream &f, int i, int j)
{
	//print_mutex.lock();
	mpreal c = speedOfLight;
	mpreal pi = M_PI;
	f << data.beam.startPoint.x << "	" << data.beam.startPoint.y /*<< "	" << data.refractiveIndex << "	" << data.beam.lambda*/ << std::endl;
	/*f << "x" << i + 1 << j + 1 << " = " << data.beam.startPoint.x << std::endl;
	f << "y" << i + 1 << j + 1 << " = " << data.beam.startPoint.y << std::endl;
	/*f << "v" << i + 1 << j + 1 << " = (" << data.beam.v.x << ", " << data.beam.v.y << ")" << std::endl;
	f << "lambda" << i + 1 << j + 1 << " = " << data.beam.lambda * pow(10, 9) << std::endl;
	f << "w" << i + 1 << j + 1 << " = " << 2 * pi*c / data.beam.lambda << std::endl;
	f << "k" << i + 1 << j + 1 << " = " << 2 * pi / data.beam.lambda << std::endl;
	f << "n" << i + 1<< j + 1 << " = " << data.refractiveIndex << std::endl;
	f << "t" << i + 1 << j + 1 << " = " << data.time << std::endl;*/
	//print_mutex.unlock();
}

