#include "Vector2.h"

Vector2::Vector2()
{
}

Vector2::Vector2(mpreal x, mpreal y) : x(x), y(y)
{
}

//dotproduct
mpreal Vector2::dot(const Vector2 &v1, const Vector2 &v2) {
	return (v1.x*v2.x + v1.y*v2.y);
}

//normalizing vector
Vector2 Vector2::normalize() const {
	if (module() == 0)
		return Vector2(x, y);
	else
		return Vector2(x / sqrt(x*x + y*y), y / (sqrt(x*x + y*y)));
}

//������ �������
mpreal Vector2::module() const {
	return sqrt(x*x + y*y);
}

//���������� ������ � �������������, ���������������� ���������
Vector2 Vector2::perpendiculize() {
	if (y != 0)
		return Vector2(1, -x / y).normalize();
	else
		if (x != 0)
			return Vector2(-y / x, 1).normalize();
		else
			return Vector2(0, 0);
}

//������� ������� �� �����
Vector2 Vector2::operator/ (const mpreal &a) const {
	return Vector2(x / a, y / a);
}

//��������� ������� �� �����
Vector2 Vector2::operator* (const mpreal &a) const {
	return Vector2(x * a, y * a);
}

//�������� ��������
Vector2 Vector2::operator+(const Vector2 &a) const {
	return Vector2(x + a.x, y + a.y);
}
