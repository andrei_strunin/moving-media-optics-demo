#pragma once
#include "OpticalSurface.h"
#include "Includes.h"
#include <ppl.h>

void modeling(std::vector<OpticalSurface>, std::vector<Beam>);

void dataPrint(LogInformation, std::fstream &f, int i, int j);

void processing(int i, int start, int len, std::vector<Beam> beams, std::vector<OpticalSurface> surfaces);