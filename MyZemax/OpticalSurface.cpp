#include "OpticalSurface.h"


int _sign(mpreal a) {
	if (a >= 0) 
		return 1;
	else 
		return -1;
}

OpticalSurface::OpticalSurface(SurfCoeffs coeffsNoTime, SurfType surfParameters, Vector2 surfSpeed)
	:coeffsOriginal(coeffsNoTime), surfParameters(surfParameters), surfSpeed(surfSpeed)
{
}

//��������� ���� ��� ����������� ����������� (������� � ����������� �. �. �����������, �. �. �������� "��������� ����� �� ����������� ������� � ����������� ������")
LogInformation OpticalSurface::beamPassing(Beam &beam, mpreal currentTime, mpreal currentRIndex) const {
	
	int sign; // ���� ��� Q
			  //�������� ���� ��� Q
	switch (surfParameters.kindOfSurface)
	{
	case Characteristic::Common:
	{
		sign = 1;
		break;
	}
	case Characteristic::Mirror:
	{
		sign = -1;
		break;
	}
	case Characteristic::Ending:
	{
		throw 228;
		break;
	}
	}

	//���� �������� �������
	const SurfCoeffs surfCoeffs = currentTimeRecording(currentTime);
	//������ ���������� �����������
	mpreal refractiveIndex = _SchottFormula(beam.lambda);
	//std::cout << refractiveIndex << std::endl;
	//�������� ����� � ����� �����������
	std::tuple<Point, mpreal> timeAndPoint = timeAndPointOfCross(surfCoeffs, beam, currentRIndex);
	//����������� �������� ������-����������� ����
	beam.v = beam.v.normalize();
	mpreal c = speedOfLight; //�������� �����
	//c /= refractiveIndex;
	mpreal w = 2*M_PI*c / beam.lambda; //�������� �������
	Vector2
		normale = normalToSurface(surfCoeffs, std::get<0>(timeAndPoint)),
		tangent = normale.perpendiculize(),
		k = beam.v * 2 * M_PI / beam.lambda;
		normale = normale*(mpreal)_sign(k.y*normale.y);//���������� ������� �� ����������� �������� ����
		tangent = tangent*(mpreal)_sign(k.x*tangent.x);//���������� ����������� �� ����������� �������� ����
	Vector2
		beta = surfSpeed / c,
		betaN = normale*Vector2::dot(beta, normale),
		betaT = tangent*Vector2::dot(beta, tangent),
		kT = tangent*Vector2::dot(k, tangent),
		kN = normale*Vector2::dot(k, normale),
		uT = tangent*Vector2::dot(surfSpeed, tangent),
		uN = normale*Vector2::dot(surfSpeed, normale),
		d = kT*c / (Vector2::dot(k, uN) - w);
	mpreal
		kappa = pow(refractiveIndex, 2) - 1,
		gamma = 1 / (1 - pow(beta.module(), 2)),
		Q = ((1 + kappa*pow(gamma, 2)*(1 - pow(betaN.module(), 2)))
			- pow(d.module(), 2)*((1 - pow(beta.module(), 2)) - kappa*pow(gamma, 2)*pow((beta.module() - betaN.module()), 2))
			+ kappa*pow(gamma, 2)*Vector2::dot(d, betaT)
			*(2 * (1 - beta.module()*betaN.module()) + (1 - pow(beta.module(), 2))*Vector2::dot(d, betaT)));

	mpreal kNNew = (w - Vector2::dot(k, uN)) / c*
		((beta.module() + kappa*pow(gamma, 2)*(beta.module() - betaN.module())*(1 + Vector2::dot(betaT, d))) + sign*sqrt(Q))
		/ ((1 - pow(beta.module(), 2)) - kappa*pow(gamma, 2)*pow((beta.module() - betaN.module()), 2));
	k = normale*kNNew + kT;
	beam.startPoint = std::get<0>(timeAndPoint);
	beam.v = k.normalize();
	beam.lambda = 2 * M_PI / k.module();
	LogInformation data = LogInformation(beam, std::get<1>(timeAndPoint), refractiveIndex);
	return data;
}

//������ ���������� ����������� �� ������� �����
mpreal OpticalSurface::_SchottFormula(mpreal lamb) const {
	switch(surfParameters.kindOfSurface)
	{
	case Characteristic::Common:
	{
		lamb = lamb*pow(10, 6);
		auto &A = surfParameters._SchottCoeffs;
		//return sqrt(A[0] + A[1] * pow(lamb, 2) + (A[2] / pow(lamb, 2)) + (A[3] / pow(lamb, 4)) + (A[4] / pow(lamb, 6)) + (A[5] / pow(lamb, 8)));
		if (surfParameters._SchottCoeffs[0] == 2.9580175) return 1.7647;
		else return 1.4729;
		break;
	}
	case Characteristic::Mirror:
	{
		return 1.4729;
		break;
	}
	case Characteristic::Ending:
	{
		throw 228;
		break;
	}
	}
	
}

//����� ����������� ���� � ������������
std::tuple<Point, mpreal> OpticalSurface::timeAndPointOfCross(const SurfCoeffs surfCoeffs, const Beam &beam, mpreal n) const {
	auto &an = surfCoeffs;
	mpreal A, B, C, c, signCosPhi = 0, signSinPhi = 0, t1, t2, time, newX, newY;
	c = speedOfLight;
	signsAndPhi(beam, signCosPhi, signSinPhi);
	mpreal const &vx = surfSpeed.x;
	mpreal const &vy = surfSpeed.y;
	auto &cx = c*signCosPhi/n, &cy = c*signSinPhi/n, &l = beam.startPoint.x, &m = beam.startPoint.y;
	//������������ ����������� ��������� ������������ t 
	A = an.a11*cx*cx + 2 * an.a12*cx*cy + an.a22*cy*cy + an.a11*cx*vx + an.a12*cy*vx + an.a11*vx*vx
		+ 2 * an.a12*cx*vy + 2 * an.a22*cy*vy + 2 * an.a12*vx*vy + an.a22*vy*vy;
	B = 2 * an.a13*cx + 2 * an.a23*cy + 2 * an.a11*cx*l + 2 * an.a12*cy*l + 2 * an.a12*cx*m
		+ 2 * an.a22*cy*m + 2 * an.a13*vx + 2 * an.a11*l*vx + 2 * an.a12*m*vx + 2 * an.a23*vy
		+ 2 * an.a12*l*vy + 2 * an.a22*m*vy;
	C = an.a33 + 2 * an.a13*l + an.a11*l*l + 2 * an.a23*m + 2 * an.a12*l*m + an.a22*m*m;
	//���� ��������� - ������
	if (A == 0)
	{
		time = -C / B;
	}
	else
	//������� ����������� ���������
	{
		t1 = (-B + sqrt(B*B - 4 * A*C)) / 2 / A;
		t2 = (-B - sqrt(B*B - 4 * A*C)) / 2 / A;
		
		if (t1 <= 0)
		{
			if (t2 >= 0)
				time = t2;
			else
				time = 401;
		}
		else
		{
			if (t2 <= 0)
				time = t1;
			else
				time = (t1 < t2) ? t1 : t2;
		}
	}
	//std::cout << time << std::endl;
	//����� �����������
	newX = beam.startPoint.x + c / n*time*signCosPhi;
	//std::cout << time<< std::endl;
	newY = beam.startPoint.y + c / n*time*signSinPhi;
	//std::cout << newX << "	" <<newY<< std::endl;
	return std::make_tuple(Point(newX, newY), time);
}

//���� ����� ����� � ������������
void OpticalSurface::signsAndPhi(const Beam &beam, mpreal &signCosPhi, mpreal &signSinPhi) const { 
	Vector2 d = beam.v;
	d = d.normalize();
	signCosPhi = Vector2::dot(d, Vector2(1, 0));
	signSinPhi = _sign(d.y)*sqrt(1 - pow(Vector2::dot(d, Vector2(1, 0)), 2));
}

//������� � �����������
Vector2 OpticalSurface::normalToSurface(const SurfCoeffs surfCoeffs, const Point &crossPoint) const {
	return Vector2(
		surfCoeffs.a11*crossPoint.x + surfCoeffs.a12*crossPoint.y + surfCoeffs.a13,
		surfCoeffs.a12*crossPoint.x + surfCoeffs.a22*crossPoint.y + surfCoeffs.a23).normalize();
}

//����������� ������������� ��������� � ������ �������� ������� �� ����� ��������������� ���� 
const SurfCoeffs OpticalSurface::currentTimeRecording(mpreal t) const {
	
	mpreal const &vx = surfSpeed.x;
	mpreal const &vy = surfSpeed.y;
	return SurfCoeffs({
		coeffsOriginal.a11,
		coeffsOriginal.a22,
		coeffsOriginal.a12,
		coeffsOriginal.a13 + coeffsOriginal.a11*vx*t + coeffsOriginal.a12*vy*t,
		coeffsOriginal.a23 + coeffsOriginal.a22*vy*t + coeffsOriginal.a12*vx*t,
		coeffsOriginal.a33 + coeffsOriginal.a11*pow(vx*t, 2) + coeffsOriginal.a22*pow(vy*t, 2)
			+ 2 * coeffsOriginal.a12*vx*t*vy*t + 2 * coeffsOriginal.a13*vx*t + 2 * coeffsOriginal.a23*vy*t
		});
	/*coeffs.a13   = coeffsOriginal.a13 + coeffsOriginal.a11*vx*t + coeffsOriginal.a12*vy*t;
	coeffs.a23 = coeffsOriginal.a23 + coeffsOriginal.a22*vy*t + coeffsOriginal.a12*vx*t;
	coeffs.a33 = coeffsOriginal.a33 + coeffsOriginal.a11*pow(vx*t, 2) + coeffsOriginal.a22*pow(vy*t, 2)
		+ 2 * coeffsOriginal.a12*vx*t*vy*t + 2 * coeffsOriginal.a13*vx*t + 2 * coeffsOriginal.a23*vy*t;*/

}