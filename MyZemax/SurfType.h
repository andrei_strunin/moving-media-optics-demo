#pragma once
#include "Includes.h"


using SchottCoeffs = std::array<mpreal, 6>;

enum class Characteristic
{
	Common, Mirror, Ending
};

class SurfType
{
public:
	Characteristic kindOfSurface; //����������/������������/�������� �����������
	SchottCoeffs _SchottCoeffs; //������������ ������� �����
	SurfType(Characteristic, SchottCoeffs);
};