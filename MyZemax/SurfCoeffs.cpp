#include "SurfCoeffs.h"

SurfCoeffs::SurfCoeffs(std::array<mpreal, 6> coeffs)
{
	this->a11 = coeffs[0];
	this->a22 = coeffs[1];
	this->a12 = coeffs[2];
	this->a13 = coeffs[3];
	this->a23 = coeffs[4];
	this->a33 = coeffs[5];
}


