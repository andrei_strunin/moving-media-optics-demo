#pragma once
#include "Includes.h"

///<summary>a11*x^2+a22*y^2+2*a12*x*y+2*a13*x+2*a23*y+a33=0</summary>
class SurfCoeffs
{
public:
	mpreal a11, a22, a33, a12, a13, a23;
	SurfCoeffs(std::array<mpreal, 6> coeffs);
};

